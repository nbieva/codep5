---
title: Travailler avec du son
lang: fr-FR
---

# Travailler avec du son

Le son, comme les images, se charge dans P5js avec la fonction preload. Ceci assure que le fichier (MP3 par exemple) soit chargé avant que le programme ne s'exécute.

Le web est plein de [sons à télécharger](https://lasonotheque.org/detail-1927-cloche-de-boxe-2.html) et utiliser dans vos projets.

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/WorkshopsB1/embed/LdJz6kH7R"></iframe>

## Charger un son et le jouer à tel ou tel moment

Avec la même logique, vous pouvez faire une balle qui émet un son quand elle rebondit sur un bord de votre sketch.

```javascript
// On déclare la variable
var monSon; 

// On charge le fichier son
function preload(){
	monSon = loadSound("mes-sons/1927.mp3");
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(0);
    // On le joue
    monSon.play();
}

function mousePressed() {
    if ( monSon.isPlaying() ) { 
        // On le met en pause
        monSon.pause();
        background(255);
    } else {
        // On le joue
        monSon.play(); 
        background(0);
    }
}
```

La fonction setVolume vous permet de gérer le volume de chacun de vos sons indépendamment les uns des autres

```javascript
monSon.setVolume(0.5);
```

La fonction loop() permet, elle, de jouer un son en boucle.

```javascript
monSon.loop();
```

## Générer un visuel en fonction du son (Mesurer l'amplitude)

+ [https://p5js.org/examples/sound-measuring-amplitude.html](https://p5js.org/examples/sound-measuring-amplitude.html)
+ [https://www.lyceelecorbusier.eu/p5js/?p=3072](https://www.lyceelecorbusier.eu/p5js/?p=3072) et [Gestion du son](https://www.lyceelecorbusier.eu/p5js/?p=2081)
+ Pour aller plus loin (1): [http://wiki.t-o-f.info/P5js/VisualisationDuSon](http://wiki.t-o-f.info/P5js/VisualisationDuSon) (de [http://wiki.t-o-f.info/](http://wiki.t-o-f.info/)) et ici directement [les slides de Jason Sigal](http://slides.com/jasonsigal/deck)
+ Pour aller plus loin (2): [Visualizing Music with p5.js](https://therewasaguy.github.io/p5-music-viz/)

::: warning
Notez que, comme pour les images, votre fichier son doit **absolument** se trouver dans votre dossier (à la racine, ou dans un dossier 'assets' ou 'mes-sons' par exemple..).
:::