---
title: Charger un fichier SVG
lang: fr-FR
---

# Charger et utiliser un SVG

Un fichier SVG (tracé vectoriel), se charge dans P5js avec la fonction preload. Ceci assure que le fichier SVG soit chargé avant que le programme ne s'exécute.

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/WorkshopsB1/embed/VwIhnPJRJ"></iframe>

```javascript
// On déclare la variable
var monSvg; 

// On charge le fichier SVG
function preload(){
	monSvg = loadImage("belgique.svg");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(255,10);
  // On le positionne à partir de son centre
  imageMode(CENTER);
  // On le dessine dans le canvas (les deux derniers paramètres étant facultatifs, pour modifier largeur et hauteur)
  image(monSvg,mouseX, mouseY, 100,70);
}
```

::: warning
Notez que votre fichier SVG doit **absolument** se trouver dans votre dossier (à la racine, ou dans un dossier 'assets' par exemple..).
:::