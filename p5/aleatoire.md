---
title: Valeurs aléatoires
lang: fr-FR
---

# Valeurs aléatoires

La fonction [**random()**](https://p5js.org/reference/#/p5/random) de *P5js* est une fonction renvoyant une valeur numérique (un nombre à virgule flottante en l'occurrence) et peut donc être utilisée tant pour la couleur que pour modifier les coordonnées ou la taille d'un élément, l'épaisseur d'un contour, la taille d'un texte, etc..

Voici un exemple de sketch utilisant la fonction [**random()**](https://p5js.org/reference/#/p5/random). Cette fonction renvoie un nombre aléatoire dans une fourchette donnée :

<iframe class="p5embed" height="450" src="https://editor.p5js.org/WorkshopsB1/full/vX3LyAQXb"></iframe>

```javascript
var r;
var v;
var b;
var a;

var longueur;
var epaisseur;
var posX;
var posY;

function setup() {
    createCanvas(750,400);
}
function draw() {
    r = random(255);
    v = random(255);
    b = random(255);
    a = random(255);
    longueur = random(100);
    epaisseur = random(10);
    posX = random(-50,width);
    posY = random(height);

    strokeWeight(epaisseur);
    stroke(r,v,b,a);
    line(posX, posY, posX+longueur, posY);
}
```

Plus d'infos sur la fonction random(): [https://p5js.org/reference/#/p5/random](https://p5js.org/reference/#/p5/random)

## random()

Voyons différents cas de figure pour la fonction random() :

```javascript
random(0,100); // Renverra un nombre entre 0 et 100
random(100); // Idem, version raccourcie
random(50,100); // Renverra un nombre entre 50 et 100
random(-50,150); // Renverra un nombre entre -50 et 150
random(width); // Renverra un nombre entre 0 et la largeur de votre sketch
```
Notez que la fonction **random() renverra toujours un nombre à virgule flottante (ex: 2.5358967)**

Pour arrondir, si nécessaire, à un nombre entier (integer), nous utiliserons les fonction **floor()** (arrondir à l'entier inférieur), **ceil()** (arrondir à l'entier supérieur), ou **round()** (arrondir à l'entier le plus proche). Par exemple :

```javascript
floor(random(50)); // Renverra un entier entre 0 et 50
```

```javascript
var nombre = 325;

function setup() {
    createCanvas(750, 400);
    noFill();
    smooth();
    //frameRate(2);
}

function draw() {
    stroke(0,140);
    background(230);
    for (var i=0; i < height; i+=5) {
        bezier(0, i, random(nombre), random(nombre), random(nombre), random(nombre), width, i);
    }
}
```

<iframe class="p5embed"  width="770" height="450" src="https://editor.p5js.org/nicobiev/full/H1jjpSD5Q"></iframe>

## noise()

La fonction **noise()**, plutôt que de renvoyer des valeurs totalement aléatoires, renverra chaque fois des valeurs liées aux précédentes. De ce fait, les transitions sembleront continues. On parle ici de [bruit de Perlin](https://www.google.be/search?q=bruit+de+perlin&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjV94PTt_TdAhXMsKQKHYIpDMoQ_AUIDigB&biw=1277&bih=539) (2 ou 3 dimensions), du nom du fondateur du Media Research Lab de NYU, [Ken Perlin](https://fr.wikipedia.org/wiki/Ken_Perlin). On utilise cette technique notamment pour modéliser des textures de reliefs, de textiles ou de nuages par exemple.

<a data-fancybox title="Noise" href="/assets/noise.png">![Noise](/assets/noise.png)</a>
> [https://www.youtube.com/watch?v=qxJryrxSIQI](https://www.youtube.com/watch?v=qxJryrxSIQI)

----

### En savoir plus :

+ [https://www.youtube.com/watch?v=IKB1hWWedMk](https://www.youtube.com/watch?v=IKB1hWWedMk)

-----

Notez qu'il n'y a pas de véritable hasard sur un ordinateur. Voici ce qu'en disent Jean-Noël Lafargue et Jean-Michel Géridan, dans leur livre *"Processing, Le code comme outil de création"* :

> Tous les ordinateurs modernes embarquent une horloge interne qui permet au système de déterminer le nombre de millisecondes qui s'est écoulé depuis une date donnée (typiquement, le 1er janvier 1970). En interrogeant ce chiffre pendant l'exécution d'un programme et en le soumettant à un algorithme dédié, on peut obtenir un chiffre pseudo-aléatoire. Il ne s'agit pas réellement de hasard puisqu'en théorie, si on exécutait deux programmes en même temps sur deux ordinateurs dont l'horloge est réglée sur la même heure au limmième de seconde près, on obtiendrait le même résultat.

Ce type de hasard est tout de même suffisant dans de nombreux cas.