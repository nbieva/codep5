---
title: Structure d'un programme
lang: fr-FR
---

# Structure d'un programme

<a data-fancybox title="" href="/assets/workshop-group2.057.jpeg">![](/assets/workshop-group2.057.jpeg)</a>

> Comme dans n'importe quel dessin, l'ordre des opérations est important pour déterminer le rendu final. Ce principe s'applique même en dehors de Processing: « S'habiller, sortir, aller au travail » ne donnera pas le même résultat que « sortir, aller au travail, s'habiller ». (Floss Manuals, Processing)

## Quelques règles de base

* Créez **votre compte** sur [p5js/signup](https://editor.p5js.org/signup)
* Créez **votre sketch**
* **Commentez** votre code (pas obligatoire mais souvent très pratique)
* Toujours un **point-virgule** à la fin d'un instruction
* Utilisez l'**indentation**. Cela rendra votre code plus lisible, pour vous-même et pour les autres.
* Soyez attentifs aux détails
* **Inspirez-vous** du code d'autres
* **Utilisez la référence p5js** ([par ici…](https://p5js.org/reference/))
* Sauvez vos sketchs en ligne
* Partagez!


Créez-vous un document (pourquoi pas partagé…) dans lequel vous rédigez votre propre documentation.

## setup()

Dans cette "section" de votre programme, vous allez initialisez une série de choses (taille de votre sketch, couleur de fond, épaisseur des contours…) et définir un "état de départ" de votre programme.
Cette section s'exécute une seule fois, de haut en bas, dans l'ordre.

```javascript
function setup() {
    // Mon code, qui se jouera UNE SEULE fois
    createCanvas(600,400);
}
```

<a data-fancybox title="" href="/assets/setupdraw.png">![](/assets/setupdraw.png)</a>


## draw()

La boucle de P5js, bien que facultative, deviendra vite indispensable pour des dessins complexes ou des animations. Elle se répète indéfiniment (même si cela ne se voit parfois pas…), tant qu'une condition ou une instruction ne vient pas lui dire de s'arrêter.



```javascript
function draw() {
    ellipse(mouseX, mouseY, 30, 30);// Mon code, qui se jouera EN BOUCLE, sauf instruction contraire
}
```
Elle est très rapide.
Vous pouvez vous en rendre compte en intégrant ceci à la boucle:

```javascript
print(frameCount);
```
Cette instruction va envoyer à la console la valeur de la variable **frameCount** qui compte le nombre de boucles.

Vous pouvez la ralentir en définissant un nombre de frames par seconde:

```javascript
frameRate(2);
```

Vous pouvez également la mettre "en pause" ou la relancer avec les fonctions **noLoop()** et **loop()**

## maFonction()

Vous pouvez également définir des fonctions personnalisées, auxquelles vous donnez un nom. Vous pouvez ensuite "appeler" ces fonctions dans la boucle de votre programme, par exemple. Ceci peut vous permettre de rendre votre code plus efficace et de ne pas vous répéter inutilement. Vous trouverez plus d'informations sur ceci [sur cette page](/p5/fonctions.html).

```javascript
// Remarquez que chaque fonction est un bloc séparé, avec une même syntaxe.

function setup() {
    // Code qui se jouera UNE SEULE fois
    createCanvas(600,400);
    noStroke();
}

function draw() {
    //Code, qui se jouera EN BOUCLE, sauf instruction contraire.
    leNomQueJeVeux();
}

function leNomQueJeVeux() {
    // Code, qui se jouera quand il sera appelé (dans le draw par exemple. Voir ci-dessus.)
    fill(255,0,0);
    rectMode(CENTER);
    rect(width/2, height/2, 100, 100);
}
```
