---
title: Les conditions
lang: fr-FR
---

# Les conditions

> Source: [Floss manuals](https://fr.flossmanuals.net/processing/les-conditions/)

Les conditions donnent une part d'autonomie à votre ordinateur. Elles lui permettent de **modifier le comportement du programme en fonction de diverses conditions de votre choix**.

Par exemple, si vous vouliez changer l'apparence de votre programme en fonction de l'heure, vous pourriez lui demander d'avoir un fond noir entre 10 heures du soir et 6 heures du matin et un fond blanc le reste du temps. C'est ce questionnement — « Quelle heure est-il ? » — qui constitue la condition. « S'il fait nuit, je dois dessiner un fond noir, sinon je dessine un fond blanc » pourrait constituer en quelque sorte le dialogue interne de *P5js* lorsqu'il rencontre une condition.

<a data-fancybox title="Conditions" href="/assets/conditions5.png">![Conditions](/assets/conditions5.png)</a>

## Comparaison

La base de la condition, c'est la comparaison. Avant de pouvoir agir selon une condition, il faut d'abord formuler la question que *P5js* doit se poser. Cette question sera quasiment toujours une question de comparaison.

<a data-fancybox title="Conditions" href="https://fr.flossmanuals.net/processing/les-conditions/static/Processing-Les_conditions-illustration_comparaison_chat_chien-fr-old.png">![Conditions](https://fr.flossmanuals.net/processing/les-conditions/static/Processing-Les_conditions-illustration_comparaison_chat_chien-fr-old.png)</a>

Si le résultat de la question est « oui », *P5js* exécutera une suite d'instructions. Si la réponse est non, il en exécutera une autre. Dans *P5js*, ce oui/non s'écrit « true » et « false ».

La syntaxe d'une condition est la suivante: if (TEST) { }. Le TEST correspond à l'opération (égalité, plus petit, plus grand) que vous aurez choisie pour comparer deux valeurs et déterminer si la réponse à la question est true ou false. Si la réponse est true, *P5js* exécutera les instructions entre les deux accolades. L'instruction else permet de gérer le cas de figure dans lequel la condition n'est pas validée. Elle exécute elle aussi tout ce qui se trouve à l'intérieur de ses accolades. Vous pouvez mettre autant d'instructions que vous voulez entre ces deux types d'accolades.

## Egalité
Pour vérifier l'égalité de deux valeurs, on utilise la formule suivante: if (valeur1 == valeur2) { }. L'exemple suivant écrit "Il est midi" dans la console si la méthode hour() donne la valeur 12.
```javascript
if (hour() == 12) {
    print("Il est midi !");
} else {
    print("Il n’est pas midi !");
}
```
Résultat de l'application exécutée entre 12h00 et 12h59 est :

```javascript
    Il est midi !
```

## Plus petit que et plus grand que
On peut vérifier qu'une valeur est plus petite ou plus grande qu'une autre en utilisant les opérateurs < et >. L'exemple suivant va écrire dans la console si nous sommes le matin ou non.
```javascript
if (hour() < 12) {
    print("C’est le matin !");
} else {
    print("Ce n’est pas le matin !");
}
```
Résultat de l'application exécutée après 12h59 :

```javascript
    Ce n’est pas le matin !
```

## Combiner les décisions

Les if et else peuvent être combinés pour gérer plusieurs cas de figure.
```javascript
if (hour() < 12) {
    print("C’est le matin !");
} else if (hour() == 12) {
    print("Il est midi !");
} else {
    print("Ce n’est pas le matin !");
}
```
Résultat de l'application exécutée avant 12h00 :

```javascript
    Ce n’est pas le matin !
```

## Combiner les tests

Plusieurs tests peuvent être combinés au sein d'une même décision pour rendre le choix plus précis. Les opérateurs && (et) ainsi que || (ou) permettent de combiner des tests. Par exemple pour déterminer si nous sommes la nuit ou le jour, nous avons besoin de trier les heures qui sont tard le soir et tôt le matin de celles du reste de la journée :
```javascript
if (hour() < 6 && hour() > 20) {
    print("Il fait nuit !");
} else {
    print("Il ne fait pas nuit !");
}
```
Résultat de l'application exécutée à 16h50 :

```javascript
    Il ne fait pas nuit !
```
