---
title: Les coordonnées
lang: fr-FR
---
# Les coordonnées

Dans P5js, le **point d'origine** (0,0) du système de coordonnées est le coin supérieur gauche de votre fénêtre.
Pour positionner un point au centre de votre sketch, vous pouvez utiliser les variables natives *width* et *height*.

```javascript
point(width/2, height/2);
```
Une forme simple (comme un rectangle) sera tracé à partir de son coin supérieur gauche. Le comportement est exactement le même que dans Illustrator par exemple.
Le code suivant ne placera donc PAS mon carré au centre du sketch:

```javascript
function setup() {
    createCanvas(500,500);
    rect(width/2, height/2, 100, 100);
}
```
Pour ce faire, je peux utiliser la fonction rectMode() comme suit:
```javascript
function setup() {
    createCanvas(500,500);
    rectMode(CENTER); // notez les capitales!
    rect(width/2, height/2, 100, 100);
}
```
Ceci produira donc le même effet que si vous appuyez sur Alt en traçant votre rectangle dans Illustrator.

<a data-fancybox title="" href="/assets/coordonnees-1.png">![](/assets/coordonnees-1.png)</a>
<a data-fancybox title="" href="/assets/coordonnees-2.png">![](/assets/coordonnees-2.png)</a>
<a data-fancybox title="" href="/assets/coordonnees-3.png">![](/assets/coordonnees-3.png)</a>
