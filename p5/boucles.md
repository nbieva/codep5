---
title: Les boucles
lang: fr-FR
sidebarDepth: 1
---

# Les répétitions, ou boucles

>Source: [Floss manuals](https://fr.flossmanuals.net/processing/les-repetitions/)

Les répétitions, ou boucles, permettent d'exécuter une série d'instructions plusieurs fois de suite. Elles évitent de dupliquer inutilement des portions de code. Attention ! les répétitions ne permettent pas de créer des animations dans le temps (d'autres instructions existent pour cela) ! Lorsque l'ordinateur lit le programme et rencontre une boucle, il va exécuter instantanément autant de fois de suite le code écrit dans le bloc de la boucle qu'on lui a indiqué.

<a data-fancybox title="Boucles" href="/assets/for.jpg">![Boucles](/assets/for.jpg)</a>

L'exemple ci-dessous va nous permettre d'illustrer simplement cette notion. L'objectif est de réaliser un dessin affichant dix lignes noires horizontales. Le premier code contient dix fois l'instruction line(), le second code est réalisé à partir d'une boucle. Le résultat des deux codes est le même, la différence se situant au niveau de la longueur du code, l'un étant plus rapide à saisir (et ultérieurement à modifier) que l'autre.

<a data-fancybox title="dessin_10lignes" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-dessin_10lignes-fr-old.png">![dessin_10lignes](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-dessin_10lignes-fr-old.png)</a>

```javascript
line(0, 0, 100, 0);
line(0, 10, 100, 10);
line(0, 20, 100, 20);
line(0, 30, 100, 30);
line(0, 40, 100, 40);
line(0, 50, 100, 50);
line(0, 60, 100, 60);
line(0, 70, 100, 70);
line(0, 80, 100, 80);
line(0, 90, 100, 90);
```

...ou plus simplement:

```javascript
for (var i = 0; i < 100; i = i + 10) {
    line(0, i, 100, i);
}
```

## La boucle for

Ce type de boucle permet de répéter une série d'instructions un nombre de fois défini. Elle incorpore une variable qui va s'incrémenter à chaque passage dans la boucle. On utilise souvent i comme nom pour la variable interne de la boucle. Elle comprend : un nombre de départ, un nombre maximal et une incrémentation. Sa syntaxe est la suivante : for (var i = NombreDeDépart; i < NombreMaximal; i = i + INCREMENT) { }.

> Note: La deuxième partie de cette déclaration ne concerne pas forcément un nombre maximal. Il s'agit tout simplement d'une condition. Tant que la condition est remplie, la boucle se rejoue.

L'exemple ci-dessous va afficher des rectangles blancs côte à côte qui auront chacun 10 pixels de large sur toute la largeur de l'espace de dessin. Ils seront espacés de 5 pixels. Nous allons afficher le premier rectangle aux coordonnées 0,0. Les suivants seront affichés aux coordonnées 15,0 puis 30,0 et ainsi de suite. Notre boucle va incrémenter sa variable de 15 pixels à chaque étape. Comme nous voulons remplir toute la largeur de l'espace de dessin, la valeur maximale sera égale à la largeur (width) de cette fenêtre de visualisation.

<a data-fancybox title="Screen_shot_2010_09_08_at_14.23.52" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Screen_shot_2010_09_08_at_14-23-52-fr-old.png">![Screen_shot_2010_09_08_at_14.23.52](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Screen_shot_2010_09_08_at_14-23-52-fr-old.png)</a>

```javascript
for (var i = 0; i < width; i = i + 15) {
    rect(i, 0, 10, 10);
}
```

## Les compteurs

Jusqu'à maintenant, nous avons utilisé les boucles for de manière à ce que la variable interne de la boucle soit directement exploitable. Dans l'exemple précédent, elle nous donne immédiatement la valeur exacte de la position sur l'axe x du rectangle à dessiner.

Les boucles for peuvent aussi être utilisées comme des compteurs. On leur donne un minimum, un maximum et on incrémente la variable seulement de 1 à chaque étape de la boucle : ceci permet de compter le nombre de fois que les instructions seront exécutées, d'où l'expression de compteur.

Cette méthode implique davantage de calculs mathématiques pour exploiter la variable au sein de la boucle.

<a data-fancybox title="Boucle3" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle3-fr-old.png">![Boucle3](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle3-fr-old.png)</a>

```javascript
for (var i = 0; i < 5; i = i + 1) {
    rect(i, 0, 10, 10);
}
```
Nous voyons dans l'exemple ci-dessus que les rectangles sont empilés les uns sur les autres. Puisque notre variable i aura des valeurs de 0 à 5 et que nous l'utilisons telle quelle pour placer les rectangles dans l'espace, ceux-ci seront placés aux coordonnées 0,0 ; 1,0; 2,0; 3,0; 4,0. Seul le dernier sera entièrement visible. Pour obtenir le même résultat que l'exemple précédent, il faut multiplier la variable. Dans le cas présent nous allons la multiplier par 15 (10 pour la largeur du rectangle et 5 de marge). Ils sont ainsi disposés tous les 15 pixels. Nous pourrions également utiliser le chiffre du compteur pour réaliser d'autres opérations.

<a data-fancybox title="Boucle4" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle4-fr-old.png">![Boucle4](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle4-fr-old.png)</a>

```javascript
for (var i = 0; i < 5; i = i + 1) {
    rect(i * 15, 0, 10, 10);
}
```

## Imbriquer des boucles

Les boucles peuvent s'imbriquer les une dans les autres. Cette technique permet de rapidement passer à des visualisations à deux, voir trois dimensions. Lorsqu'on imbrique des boucles, il faut prendre garde au nom que l'on donne à la variable de chaque boucle. En effet si elles se nomment toutes i, le programme va mélanger les boucles. Chaque variable de chaque boucle doit avoir un nom propre. Par exemple : i, j, k, etc. ou si elles sont liées à des dimensions : x, y et z.

Boucle de départ

<a data-fancybox title="Boucle5" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle5-fr-old.png">![Boucle5](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle5-fr-old.png)</a>

```javascript
translate(7, 7);

for (var x = 0; x < 6; x = x + 1) {
    rect(x * 15, 0, 10, 10);
}
```

## Deux boucles
<a data-fancybox title="Boucle6" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle6-fr-old.png">![Boucle6](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle6-fr-old.png)</a>

```javascript
translate(7, 7);
//Première boucle (hauteur)
for (var y = 0; y < 6; y = y + 1) {
    //Seconde boucle (largeur)
    for (var x = 0; x < 6; x = x + 1) {
        rect(x * 15, y * 15, 10, 10);
    }
}
```

## Trois boucles
Dans cet exemple nous introduisons un espace 3D. Pour placer nos rectangles dans la profondeur, nous devons utiliser la méthode translate().

<a data-fancybox title="Boucle7" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle7-fr-old.png">![Boucle7](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-Boucle7-fr-old.png)</a>

```javascript
createCanvas(100, 100, WEBGL);

translate(7, 7);

//Première boucle (profondeur)
for (var z = 0; z < 6; z = z + 1) {
    translate(0, 0, z * -15); //On recule l'objet sur l'axe z
    //Seconde boucle (hauteur)
    for (var y = 0; y < 6; y = y + 1) {
        //Troisième boucle (largeur)
        for (var x = 0; x < 6; x = x + 1) {
            rect(x * 15, y * 15, 10, 10);
        }
    }
}
```

## Variations
Voici une série de variations des exemples ci-dessous qui utilisent les méthodes fill(), scale() ou rotate(). A vous d'expérimenter les transformations au sein de boucles.

<a data-fancybox title="samples" href="https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-samples-fr-old.png">![samples](https://fr.flossmanuals.net/processing/les-repetitions/static/Processing-Les_r__p__titions-samples-fr-old.png)</a>
