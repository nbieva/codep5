---
title: Remises des travaux et feedback
lang: fr-FR
---

# Remise de vos travaux

<p class="lead">En règle générale, essayez de nous rendre tout ce que vous réalisez durant cette journée, ou du moins tout ce que vous pensez être significatif.</p>

La procédure pour la remise de vos différents sketches est reprise sur [cette page](/generalites/creation-compte), dont nous avons parlé en matinée.

En fin de journée, ne partez surtout pas sans vous être assuré que les URLs de vos travaux sont bien reprises dans notre [feuille de calcul sur le Drive](https://docs.google.com/spreadsheets/d/15xHfCjfxhVV9EGBQqxm8MbZpYzrxXycIHRpP8I5qosc/edit?usp=sharing) avec vos nom et prénom pour chacune d'elles.


<a href="https://docs.google.com/spreadsheets/d/15xHfCjfxhVV9EGBQqxm8MbZpYzrxXycIHRpP8I5qosc/edit#gid=0" target="_blank" style="display:inline-flex;border-radius:4px;margin:1rem 0;color:white;background:#ed225d;padding:1rem 1.25rem;text-decoration:none;">Cliquez ici pour ajouter vos travaux</a>

<!-- 2. vous avez rempli le **formulaire de feedback ci-dessous**. Ce formulaire est très important pour nous et nous permet d'ajuster le contenu des prochaines workshops en fonction de vos retours.

# Feedback

Le formulaire ci-dessous est anonyme. Il nous permet néanmoins d'identifier les éventuels problèmes rencontrés et améliorations à apporter pour ces workshops. Les commentaires positifs sont également les bienvenus!!

<iframe style="margin-top:30px;" src="https://docs.google.com/forms/d/e/1FAIpQLSf3Kc3DdoZjI8c7eD_QDtyDp_tNPod_vTdJtgD4wOmyCn75UA/viewform?embedded=true" width="740" height="1484" frameborder="0" marginheight="0" marginwidth="0">Chargement…</iframe> -->



<iframe  width="740" height="700" frameborder="0" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQf8bd95rZH-AbfDrcTJ4du_4n3dT4aaVVUkTTK0F6T87dxs-8QV8t9_uv0KMktN39oUto7u7bBQM4F/pubhtml?widget=true&amp;headers=false"></iframe>