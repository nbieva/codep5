---
title: Feedback
lang: fr-FR
---

# Feedback

Le formulaire ci-dessous est anonyme. Il nous permet néanmoins d'identifier les éventuels problèmes rencontrés et améliorations à apporter pour ces workshops. Les commentaires positifs sont également les bienvenus!! La dernière question est la plus importante pour nous..

<iframe style="margin-top:30px;" src="https://docs.google.com/forms/d/e/1FAIpQLSf3Kc3DdoZjI8c7eD_QDtyDp_tNPod_vTdJtgD4wOmyCn75UA/viewform?embedded=true" width="740" height="1484" frameborder="0" marginheight="0" marginwidth="0">Chargement…</iframe>