// .vitepress/theme/index.js
import DefaultTheme from 'vitepress/theme'
import './custom.scss'
import Video from '../components/Video.vue'
import Site from '../components/Site.vue'
import Image from '../components/Image.vue'

export default {
    ...DefaultTheme,
    enhanceApp(ctx) {
      DefaultTheme.enhanceApp(ctx)
      ctx.app.component('Video', Video)
      ctx.app.component('Site', Site)
      ctx.app.component('Image', Image)
    }
  }