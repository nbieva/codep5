export default {
    title: 'Code B1',
    titleTemplate: 'Arts numériques',
    description: 'Support des workshops B1',
    head: [
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible:ital,wght@0,400;0,700;1,400;1,700&display=swap' }]
  ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav: [
          { text: 'Reference', link: 'https://p5js.org/reference/' },
          { text: 'Éditeur en ligne', link: 'https://editor.p5js.org/' }
          /* {
            text: 'Dropdown Menu',
            items: [
              { text: 'Item A', link: '/item-1' },
              { text: 'Item B', link: '/item-2' },
              { text: 'Item C', link: '/item-3' }
            ]
          } */
        ],
        sidebar: [
            {
              text: 'Généralités',
              collapsible: true,
                collapsed: false,
              items: [
                { text: 'Introduction', link: '/generalites/introduction' },
                { text: "Filiation", link: 'generalites/filiation' },
                { text: "Exemples", link: 'generalites/inspirations' },
                { text: "Scratch & Micro:bit", link: 'generalites/scratch' },
                { text: "Processing & P5js", link: 'generalites/processing-et-p5js' },
                { text: "Le monde physique", link: 'generalites/monde-physique' },
                { text: "Adresses utiles", link: 'generalites/adresses' },
                { text: "Mise en place p5js", link: 'generalites/creation-compte' },
                { text: "Pour commencer..", link: 'generalites/commencer' }
              ]
            },
            {
              text: 'P5*js',
              collapsible: true,
              collapsed: false,
              items: [
                { text: "Structure d'un programme", link: 'p5/structure' },
                { text: "Les coordonnées", link: 'p5/coordonnees' },
                { text: "Les formes simples", link: 'p5/formes-simples' },
                { text: "La couleur", link: 'p5/couleur' },
                { text: "Valeurs aléatoires", link: 'p5/aleatoire' },
                { text: "Les variables", link: 'p5/variables' },
                { text: "Les conditions", link: 'p5/conditions' },
                { text: "Les boucles", link: 'p5/boucles' },
                { text: "Méthodes", link: 'p5/fonctions' },
                { text: "Les transformations", link: 'p5/transformations' },
              ]
            },
            {
              text: 'En pratique',
              collapsible: true,
              collapsed: true,
              items: [
                { text: "Animer des éléments", link: 'en-pratique/animation' },
                { text: "Travailler avec des images", link: 'en-pratique/images' },
                { text: "Charger un fichier SVG", link: 'en-pratique/charger-svg' },
                { text: "Travailler avec du son", link: 'en-pratique/son' },
                { text: "Interactivité souris", link: 'en-pratique/interactivite-souris' },
                { text: "Interactivité clavier", link: 'en-pratique/interactivite-clavier' },
                { text: "Travailler avec du texte", link: 'en-pratique/texte' },
                { text: "La vidéo", link: 'en-pratique/film' },
                { text: "Exporter", link: 'en-pratique/exporter' }              ]
            },
            {
                text: 'Exercices',
                collapsible: true,
                collapsed: true,
                items: [
                  { text: "Formes simples", link: 'exercices/formes-simples' },
                  { text: "Générez des formes", link: 'exercices/generatif' },
                  { text: "Un outil de dessin..", link: 'exercices/outil-de-dessin' },
                  { text: "Trames et motifs", link: 'exercices/trames' },
                  { text: "10print", link: 'exercices/10print' },
                  { text: "Créez une horloge", link: 'exercices/horloge' },
                  { text: "Slit scanning", link: 'exercices/slitscan' }
                ]
              },
              {
                text: 'Pour clôturer',
                collapsible: true,
                collapsed: true,
                items: [
                  { text: "Remise des travaux", link: 'cloture/rendus' }
                ]
              }
          ]
      }
  }