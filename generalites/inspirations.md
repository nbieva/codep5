# Exemples

## Louis Eveillard

Couvertures génératives > https://louiseveillard.com/projets/couvertures-generatives
Charte graphique créée par Coraline Mas-Prévost, programme de génération créé par Louis Eveillard.

Un projet pour redonner à des livres numériques les couvertures qu’ils ont perdues pendant leur numérisation. Ces couvertures utilisent les méta-données issues de chaque livre. 

<a data-fancybox title="" href="https://louiseveillard.com/thumbs/projets/couvertures-generatives/couvertures_generatives-cover-1600x1167.jpg">![](https://louiseveillard.com/thumbs/projets/couvertures-generatives/couvertures_generatives-cover-1600x1167.jpg)</a>

Mais aussi ceci: [https://affichesinteractives.louiseveillard.com/](https://affichesinteractives.louiseveillard.com/)

Ou encore: [http://pca.louiseveillard.com/drawing-patterns.php](http://pca.louiseveillard.com/drawing-patterns.php)

Ce workshop avait pour objectif d’expérimenter et de fabriquer des supports hybrides à mi-chemin entre l’affiche imprimée et la page web interactive.

## Recode Project

Le Recode Project est un projet visant à "restaurer" ou "recoder" certaines des premières oeuvres des pionniers de l'art algorithmique, et ce à l'aide de Processing.

+ [http://recodeproject.com/](http://recodeproject.com/)

<a data-fancybox title="" href="/assets/recode-project0.png">![](/assets/recode-project0.png)</a>
<a data-fancybox title="" href="/assets/recode-project.png">![](/assets/recode-project.png)</a>


## [Le Tricodeur](https://louiseveillard.com/projets/le-tricodeur)

<a data-fancybox title="" href="https://louiseveillard.com/thumbs/projets/le-tricodeur/tricodeur-residence-large-1-1600x1067.jpg">![](https://louiseveillard.com/thumbs/projets/le-tricodeur/tricodeur-residence-large-1-1600x1067.jpg)</a>

<a data-fancybox title="" href="/assets/tricodeur2.png">![](/assets/tricodeur2.png)</a>

**Louis Eveillard** / Le Tricodeur explore la mise en relation de deux pratiques créatives liées entre elles par un héritage commun : le tricot et la programmation. 

## Claire Williams

+ [Site](http://www.xxx-clairewilliams-xxx.com)
+ [https://xxxclairewilliamsxxx.wordpress.com/binary-textiles/](https://xxxclairewilliamsxxx.wordpress.com/binary-textiles/)

## Zdeněk Sýkora

<a data-fancybox title="Zdeněk Sýkora" href="/assets/sykora.png">![](/assets/sykora.png)</a>

[Zdeněk Sýkora](https://www.google.com/search?q=Zden%C4%9Bk+S%C3%BDkora&client=firefox-b-d&source=lnms&tbm=isch&sa=X&ved=0ahUKEwia0K-NwsrgAhXMxqQKHad6C7kQ_AUIDigB&biw=1391&bih=897)

## Philip Stearns

<a data-fancybox title="" href="/assets/stearns.jpg">![](/assets/stearns.jpg)</a>

+ [https://phillipstearns.wordpress.com/fragmented-memory/](https://phillipstearns.wordpress.com/fragmented-memory/)

## Craft, Colours and Computation

<a data-fancybox title="" href="/assets/crafts.png">![](/assets/crafts.png)</a>

Il m'est devenu impossible de ne pas penser au calcul: une fois que vous voyez le type de motif qui commence à émerger et comment, en alternant les couleurs, vous construisez essentiellement une carte perforée allongée, vous pouvez également commencer à réfléchir à la manière dont vous pouvez encoder et programmer des messages dans le thread comme s'il s'agissait de code binaire.
[...]
Tout d'abord, je devais comprendre les règles et comment les règles étaient mappées sur les résultats des modèles réels.
[...]
Et il y a des compromis à faire: il y a moins d'appréciation de la technique, de l'effort et du métier que vous obtenez en utilisant le métier à tisser; d'autre part, vous gagnez une énorme capacité expérimentale et itérative. Si vous n'aimez pas un motif, il vous suffit de modifier les paramètres et de dire au métier de fonctionner à nouveau.

+ [https://medium.com/@piscosour/craft-colours-and-computation-30e56e56056d](https://medium.com/@piscosour/craft-colours-and-computation-30e56e56056d)
+ [https://www.exploratorium.edu/tinkering/blog/2018/05/10/weaving-roundup](https://www.exploratorium.edu/tinkering/blog/2018/05/10/weaving-roundup)
+ [http://kairotic.org/](http://kairotic.org/)

## Pixtil – Weaving generative patterns

<a data-fancybox title="" href="/assets/pixtil.png">![](/assets/pixtil.png)</a>

+ [http://pixtil.fr/php/generatif/](http://pixtil.fr/php/generatif/)

## Sigrid Calon, Letters Become Patterns, Tilburg, 2014

<a data-fancybox title="" href="/assets/calon.png">![](/assets/calon.png)</a>

+ [https://www.sigridcalon.nl/](https://www.sigridcalon.nl/)

## Zuzana Licko, Hypnopaedia

<a data-fancybox title="" href="/assets/licko.png">![](/assets/licko.png)</a>

+ [https://www.hypocritedesign.com/zuzana-licko/](https://www.hypocritedesign.com/zuzana-licko/)
+ [http://ruinsorbooks.com/2013/02/hypnopaedia-from-emigre-fonts-zuzana-licko/](http://ruinsorbooks.com/2013/02/hypnopaedia-from-emigre-fonts-zuzana-licko/)
+ [https://www.spoonflower.com/profiles/zuzana_licko](https://www.spoonflower.com/profiles/zuzana_licko)

## Type + Code, by Yeohyun Ahn and Viviana Cordova

<a data-fancybox title="" href="/assets/typo5.png">![](/assets/typo5.png)</a>

> [Type + Code: Processing For Designers](https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn) (Published on May 29, 2009)  
> 
By Yeohyun Ahn and Viviana Cordova. Type + Code, explores the aesthetic of experimental code driven typography, with an emphasis on the programming language Processing which was created by Casey Reas and Ben Fry.

## Schultzschultz

+ [https://www.schultzschultz.com/free-works.html](https://www.schultzschultz.com/free-works.html)


<Site url="https://www.schultzschultz.com/free-works.html"/>

<a data-fancybox title="" href="/assets/schultz.png">![](/assets/schultz.png)</a>

## [LAIKA font](https://laikafont.ch/)

Laika reacting to real time weather data. see live: [sf.laikafont.ch](http://sf.laikafont.ch)

<Site url="http://sf.laikafont.ch"/>

## [Possible structures](http://superficie.ink/)

<a data-fancybox title="" href="/assets/structures.png">![](/assets/structures.png)</a>

## Radiohead / House of cards

<iframe width="770" height="430" src="https://www.youtube.com/embed/8nTFjVm9sTQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

> [Lien vers le dépôt Git](https://github.com/dataarts/radiohead)


## RELENTLESS DOPPELGANGER \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/ \m/

<Video 
type="youtube"
id="MwtVkPKx3RA"
legend="Neural network generating technical death metal, via livestream 24/7 to infinity."
link="https://www.youtube.com/watch?v=MwtVkPKx3RA"
textlink=""
/>

## Travis Scott

<Video 
type="youtube"
id="3UwLhqcZqxc"
legend="TravisBott Jack Park Canny Dope Man [Official]"
/>

## Stéphane Noël

Un très beau travail de Stéphane Noël, générant un PDF à partir de films (et des fichiers de sous-titres associés).

+ [http://movieprint.codedrops.net/](http://movieprint.codedrops.net/)

<a data-fancybox title="" href="/assets/movieprint.png">![](/assets/movieprint.png)</a>

<a data-fancybox title="" href="/assets/movieprint-2.png">![](/assets/movieprint-2.png)</a>

## Marion Frebourg
<a data-fancybox title="" href="/assets/marionfrebourg.png">![](/assets/marionfrebourg.png)</a>

"La particularité du livre de Georges Perec, "La Disparition" fut d'être réalisé sans la lettre "e" (lettre la plus commune de l'alphabet). Dans le prolongement de cette intervention sur le texte, [j'ai fait disparaître au fil des pages chacune des 25 lettres de l'alphabet](https://cargocollective.com/marionfrebourg/LA-DISPARITION). En les reliant entre eux, les mots s'ancrent dans la page et perdent toute lisibilité pour tendre vers l'abstraction."

## Basil.js

<a data-fancybox title="" href="/assets/basil.jpg">![](/assets/basil.jpg)</a>

Les fondateurs de l'équipe basil.js sont: Ted Davis ,  Benedikt Groß  et  Ludwig Zeller
Fantastiques contributeurs au projet: Philipp Adrian, be: screen GmbH,  Ken Frederick ,Stefan Landsbek ,Timo Rychert  et Fabian Morón Zirfas

+ [http://basiljs.ch/about/](http://basiljs.ch/about/)

## [Réseaux antagonistes génératifs](https://fr.wikipedia.org/wiki/R%C3%A9seaux_antagonistes_g%C3%A9n%C3%A9ratifs)

<a data-fancybox title="" href="/assets/ai1.png">![ai](/assets/ai1.png)</a>

+ [Robbie Barrat](https://www.artnome.com/news/2019/1/22/ai-artist-robbie-barrat-and-painter-ronan-barrot-collaborate-on-infinite-skulls)
+ [Art42](https://art42.net/)
+ [Mario Klingemann](http://quasimondo.com/)

La métaphore de Sol LeWitt s'applique de multiples façons dans l'art GAN. L'ensemble de données est comme la carte de règles, avec des règles créées par curation - et le réseau les interprète pour faire de l'art. Mais en plus, le réseau lui-même est aussi comme la carte de règles, et les générations individuelles ne sont que des interprétations / exécutions différentes de ces règles. Cela est conforme à l'idée que les œuvres individuelles ne sont que des «jetons» de quelque chose de plus grand - ce sont des ombres du réseau, l'œuvre d'art réelle.

Dans le même temps, si le réseau lui-même est une œuvre d'art, il est très étrange, car il ne peut pas être vu ou compris entièrement (contrairement à l'ensemble des règles responsables des œuvres génératives traditionnelles). Nous ne pouvons en obtenir que de petits aperçus à la fois. Je ne connais aucun autre type d'art où cela est vrai.

Robbie Barrat

## Ai WeiWei et Olafur Eliasson : [Moon](http://www.moonmoonmoonmoon.com/)

Launched on 9 November 2013 by Olafur Eliasson and Ai Weiwei, Moon offered people around the world an opportunity to connect with others online via a collaborative drawing platform that transcended international borders to celebrate creative expression and interaction. Eliasson and Ai decided to end the project in September 2017. The moon itself, and the patchwork of drawings created during its four years of existence, can still be explored here.

"Comment Internet ouvre-t-il une ère nouvelle d’expression créative au-delà des frontières géographiques, sociales, économiques et politiques ? Tel est l’intitulé de l'intervention d'Olafur Eliasson et Ai WeiWei, intervention qui prend comme point de départ la certitude que la liberté d’expression, comme « le vent et l’air », ne peut pas être arrêtée. Les deux artistes ne dissertent pas : ils inaugurent Moon, un projet porté en commun. Il s’agit d’une « plateforme collaborative » qui prend la forme d’une planète divisée en plusieurs milliers de parcelles carrées et vierges, sur lesquelles chacun est invité à faire une « marque », sa marque, écrite et/ou dessinée." 

Source: [Arts Hebdo Medias](http://artshebdomedias.com/article/170114-ai-weiwei-et-olafur-eliasson-la-liberte-expression-en-orbite/)

<a data-fancybox title="Ai WeiWei et Olafur Eliasson" href="/assets/moonwei.png">![Marius Watz](/assets/moonwei.png)</a>

## Patience

Patience is a clock which uses a human face to represent the passage of time. The eyes work in the same way as an analog clock’s hands, with the right eye indicating hours and the left eye indicating minutes. The mouth opens and closes to represent seconds.

+ [https://vimeo.com/178717133](https://vimeo.com/178717133)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/178717133?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>

## [Marius Watz](http://mariuswatz.com/)

Object #5 is a parametric form developed with reference to previous 3D printed objects. 
The model was created in Processing and the parts CNC milled.

<a data-fancybox title="Marius Watz" href="/assets/watz.jpg">![Marius Watz](/assets/watz.jpg)</a>

## [Olivier van Herpt](http://oliviervanherpt.com/)

<a data-fancybox title="" href="http://talent.stimuleringsfonds.nl/2016/site/files/1119/olivier3.jpg">![](http://talent.stimuleringsfonds.nl/2016/site/files/1119/olivier3.jpg)</a>

[Solid vibration](http://oliviervanherpt.com/solid-vibrations/)


https://zkm.de/en/exhibition/2017/10/open-codes

<!-- ## Kyle McDonald

+ Exhausting a crowd: https://github.com/kylemcdonald/ExhaustingACrowd

<a data-fancybox title="Kyle" href="/assets/kyle1.png">![Kyle](/kyle1.png)</a>

## Golan Levin

+ Augmented Hand Series: [https://vimeo.com/111951283](https://vimeo.com/111951283)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/111951283?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script> 


## Pablo Garcia

+ [https://www.pablogarcia.org/profilograph-after-muybridge/](https://www.pablogarcia.org/profilograph-after-muybridge/)

<a data-fancybox title="" href="/assets/garcia.jpeg">![](/assets/garcia.jpeg)</a>-->

<!-- ## Iris Van Herpen

+ [https://www.irisvanherpen.com/](https://www.irisvanherpen.com/)

<a data-fancybox title="" href="/assets/vanherpen1.png">![](/assets/vanherpen1.png)</a>

<a data-fancybox title="" href="/assets/vanherpen2.png">![](/assets/vanherpen2.png)</a>

## Madison Maxey

"Afin de pouvoir participer à la mode de demain, je pense qu'il est essentiel de donnaitre un petit peu le code.
Le futur des vêtement sera tout à fait responsive à nos corps."
On parle ici de vêtements qui pourraient chauffer quand il commence à faire froid, où s'illuminer en fonction de l'intensité lumineuse de l'environnement.

[https://www.madisonmaxey.com](https://www.madisonmaxey.com)

## Pauline van Dongen

"Concept de Liquid modernity introduit par le sociologue Zygmunt Bauman. Nos environnements sont en constante évolution et en changement constant et sont devenus liquides sous l'influence de la technologie. Pour moi, le futur de la mode est dynamic, adaptive et responsive."

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/105908842?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

------

+ [Iris Van Herpen](https://www.google.com/search?q=Iris+Van+Herpen&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiJrfnA-MngAhXG_KQKHeDvDT8Q_AUIDigB&biw=1553&bih=978#imgrc=n6R0sLdIMJhtxM:)
+ [Anneke Smelik](https://hetnieuweinstituut.nl/en/anneke-smelik-cybercouture)
 -->
## Patternator

Le Patternator est un petit projet créé à l'occasion d'un workshop avec l'atelier de design textile. Il propose de créer votre propre motif à partir d'un police créée sur Fontscruct. Il utilise uniquement HTML et CSS (pour la forme) et P5*js pour la logique et les fonctionnalités.

Il est un bon exemple de ce qu'il est possible de mettre en place avec les outils vus dans vos 2 premières années d'études.

+ [https://patternator.netlify.app/](https://patternator.netlify.app/)

<a data-fancybox title="" href="/assets/patternator.png">![](/assets/patternator.png)</a>

<iframe class="p5embed" width="740" height="370" src="https://editor.p5js.org/nicobiev/embed/p0BrzZ9R"></iframe>

**(Cliquez sur l'image ci-dessus et taper sur une touche de votre clavier..)**

## Generative Design

Cette page reprend les liens vers les sketches du livre Geberative Design, récemment édité. Vous pouvez les ouvrir directement en ligne, dans l'éditeur de P5js, vous en inspirer, les modifier, etc.

> [http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)

<a data-fancybox title="" href="/assets/generative.png">![](/assets/generative.png)</a>