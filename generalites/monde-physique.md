---
title: Le monde physique
lang: fr-FR
---

# Le code et le monde physique



<Image 
file="piphone.jpg" 
legend="PiPhone" 
link="http://www.davidhunt.ie/piphone-a-raspberry-pi-based-smartphone/" 
textlink=""
/>

Il est très intéressant d'utiliser P5js ou Processing pour manipuler des fichiers numériques, créer des images, produire des sons, etc. Mais les notions évoquées durant ce workshop pourraient également nous servir pour interagir avec des éléments du monde *physique*.
Gérer des lumières sur une scène, déclencher certains événements d'une installation interactive, construire un robot, un controlleur Midi, piloter un traceur, etc..

Nous utiliserions pour ce faire certaines interface physiques comme les cartes **Arduino**, **Micro:bit** ou **Raspberry Pi**. Tous ces projets sont des projets Open-Source, souvent créés pour apprendre, donc également assez abordables.

Si vous chipotez un jour avec une de ces cartes, vous risquez de vous frotter à Wiring (langage de programmation pour les cartes Arduino/Genuino) ou Python, souvent utilisé sur les Raspberry. Les notions de bases abordées ici avec javascript seront les mêmes dans ces différents langages.

## Arduino

> [https://www.arduino.cc/](https://www.arduino.cc/)

L'idée ici sera de faire communiquer Arduino et Processing pour, par exemple, allumer une diode (ou démarrer une voiture..), quand je passe ma souris sur une image, ou que je clique sur un bouton ou, comme je l'avais vu un jour, faire sonner une cloche chaque fois qu'un nouvel article est posté sur Wikipedia. Pour bon nombre de projets, Processing ne sera même pas nécessaire. Vous migrerez alors vers d'autres langages, comme *Javascript* ou *Python*.

<Image 
file="arduino.png" 
legend="Arduino" 
link="" 
textlink=""
/>

L'interface d'Arduino est très proche de celle de processing. Le langage diffère légèrement, mais vous passerez sans souci de l'un à l'autre, après 2-3 heures de travail.

<Image 
isabsolute
file="http://fab.cba.mit.edu/classes/863.11/people/daniel.rosenberg/images/p9/p9_7.jpg" 
legend="Arduino et Processing" 
link="" 
textlink=""
/>

## MakeyMakey

Le MakeyMakey est un dispositif d'émulation de clavier à partir d'objets du quotidien : la manipulation de tout objet conducteur relié au MakeyMakey va envoyer un signal à un ordinateur, qui réagira avec la fonction que vous avez défini, en fonction du logiciel que vous utilisez.

MakeyMakey propose un détournement "Do It Yourself" de la manette de jeu et du clavier : ce que vous voulez créer comme interaction ne dépend que de vous. Facile à utiliser, sans danger, il permet une infinité d'interaction avec un ordinateur. (src: http://labenbib.fr)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/60307041?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>

## Raspberry Pi

> [https://www.raspberrypi.org/](https://www.raspberrypi.org/)

Raspberry Pi est un nano ordinateur. Il est, pour son prix (+-40 EUR), extrêmement puissant. Il s'agit ici de matériel(hardware) libre, comme l'est également la carte Arduino.

<Image 
file="raspberrypi.jpg" 
legend="Raspberry Pi" 
link="" 
textlink=""
/>