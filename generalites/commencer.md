---
title: Pour commencer
lang: fr-FR
---

# Pour commencer

Tout d'abord, veillez à avoir [mis en place votre espace de travail et créé votre compte sur l'éditeur de P5js](/generalites/creation-compte.md).

Ouvrez dans votre navigateur les trois onglets suivants et gardez-les à portée de main durant la journée:

1. **[Ce site](https://codep5.netlify.app)** (peut-être même cette page)
2. L'**[éditeur de P5js](https://editor.p5js.org/)** bien sûr
3. La **[Reference](https://p5js.org/examples/)**(documentation) de P5js.
4. Le **[fichier partagé](https://docs.google.com/spreadsheets/d/15xHfCjfxhVV9EGBQqxm8MbZpYzrxXycIHRpP8I5qosc/edit#gid=0)** où [déposer vos sketches](/generalites/creation-compte.html) (mode *Present*)
5. Votre **moteur de recherche** préféré

N'hésitez pas non plus à fouiller la section [Examples](https://p5js.org/examples/) du site de P5js ou [https://openprocessing.org/](https://openprocessing.org/)! Vous y trouverez de chouettes bases pour vos projets.

La section P5*js, dans le menu latéral, revient sur les différents éléments dont nous parlons en début de matinée et début d'après-midi (structure d'un programme, coordonnées, couleur..). N'hésitez pas à prendre le temps de parcourir ces différentes pages et de tester les différents morceaux de code que vous y trouverez.

De façon générale, retenez qu'**il vous sera toujours plus facile de travailler avec du code si vous savez ce que vous voulez réaliser**. N'hésitez donc pas à **commencer par un projet sur papier** et à nous interpeller pour évaluer sa faisabilité.

**Mieux vaut également se donner de petits challenges successifs en montant en complexité, plutôt que de se donner un objectif trop ambitieux dans lequel on risque de se perdre.**

### Un progresion logique.. 

Voici une progression logique pour commencer à travailler avec P5js (à ne pas forcément suivre. Vous êtes libres)

+ Dessin de [formes simples](/p5/formes-simples.html)
+ [Animation de formes](/en-pratique/animation.html) simples
+ Introduction de [valeurs aléatoires](/p5/aleatoire.html)
+ Interactivité ([clavier](/en-pratique/interactivite-clavier.html) ou [souris](/en-pratique/interactivite-souris.html))
+ Travailler avec des [images](/en-pratique/images.html)
+ Travailler avec du [son](/en-pratique/son.html)
+ Travailler avec du [texte](/en-pratique/texte.html)

### Pour tout ceci, vous aurez bien souvent besoin de comprendre:

+ Le système de [coordonnées](/p5/coordonnees.html)
+ Comment formuler la [couleur](/p5/couleur.html)
+ Comment fonctionnent les [variables](/p5/variables.html)
+ Le principe des [conditions](/p5/conditions.html)
+ Eventuellement les [boucles for](/p5/boucles.html)
+ Comprendre comment [sauvegarder/exporter vos sketchs au formats JPG ou PNG](/en-pratique/exporter.html#exporter-votre-canvas).

### Idées de projets concrets:

+ Créer un [outil de dessin](/exercices/outil-de-dessin.html)
+ Créer un [générateur de formes](/exercices/generatif.html)
+ Créer un jeu (essayer d'attraper un élément augmenter le score, monter d'un niveau, etc..)
+ Créer une [horloge](/exercices/horloge.html)
+ Exploiter le [Slit-scan](/exercices/slitscan.html) et la [webcam](/en-pratique/film.html)

Vous trouverez d'autres exercices ou idées dans la section **Exercices** du menu latéral.






## Un sketch de base

Voici une base de travail très simple pour, par exemple, créer un outil de dessin personnalisé.

1. Ouvrez l'éditeur de P5*js.
2. **Collez**-y le code ci-dessous.
3. **Sauvez** votre document (après vous être connecté)
4. **Exécutez** votre code en appuyant sur play.
5. **Augmentez**-le, modifiez-le petit à petit.
6. **Sauvez** régulièrement votre travail (Cmd+S)

```javascript
function setup(){
    createCanvas(800,600); 
    background(220);
}

function draw() {
    ellipse(mouseX,mouseY,60,60);
}
```

### Ce même code, commenté

```javascript
// Le code à l'intérieur du bloc setup ne s'excutera qu'une seule fois.
function setup(){
    //Définir la taille de votre sketch (Un peu comme choisir un format pour une feuille de papier)
    createCanvas(800,600); 

    //Définir une couleur de fond (pas indispensable mais on le fait souvent)
    background(220,220,220); //peut s'écrire background(220);
}

// Le code à l'intérieur du bloc draw s'excutera en boucle, jusqu'à ce qu'on lui dise de s'arrêter.
function draw() {
    //Inscrire ici (dans le draw) le code qui va tourner en boucle (variables qui évoluent, conditions, etc.)
    ellipse(mouseX,mouseY,60,60);
}
```