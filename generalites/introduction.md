>"Drawing is a verb"
> [Richard Serra](https://www.moma.org/explore/inside_out/2011/10/20/to-collect/)

<Video type="vimeo" id="74725118" />

## Le code

Le workshop est organisé sur une journée et vise à **vous familiariser** avec le code à travers une série d'explorations visuelles ou sonores.

## P5*js

Cette première approche du code, laboratoire et expérimentale, se fera à l'aide de [P5*js](https://p5js.org/), un outil créé à l'aide d'un langage dont vous avez probablement déjà entendu parler: Javascript. Il a été créé dans l'esprit de [Processing](https://processing.org/), un outil dont nous parlerons également, et en partage un grand nombre d'objectifs.

P5*js est un outil libre utilisé par un grand nombre d'artistes à travers le monde et **ne nécessite que votre navigateur préféré pour fonctionner** et partager votre travail en ligne, ce qui en fait un outil particulièrement intéressant.
Notez que vous aurez également besoin d'une connexion Internet si vous utilisez l'[éditeur en ligne](https://editor.p5js.org/), ce que nous ferons aujourd'hui.

Créé par des artistes, pour des artistes, dans les champs pédagogique et visuel, P5 va nous permettre d'entrer facilement dans les **logiques d'un algorithme** et d'en comprendre les fonctions de base (variables, boucles, conditions, fonctions, listes..)

> Plus d'info sur [Processing et P5js](/generalites/processing-et-p5js)

-----

Notre souhait est que cette journée puisse être, pour tous, une porte ouverte vers de **nouvelles possibilités**, et puisse enrichir votre travail et votre démarche personnelle, vous amenant à une **plus grande autonomie**, à la fois technique et artistique.

Il est important de noter que **ce workshop s'adresse à tous les étudiants de B1**, quelque soient leur option et leur niveau de connaissance en la matière. Il a été conçu comme tel.


<Image file="aymeraude.jpeg" legend="Le travail d'Aymeraude, option Dessin, entièrement réalisé dans Illustrator."/>
<Image file="noa.png" legend="1000 skies, le travail de Noa (option Peinture)"/>