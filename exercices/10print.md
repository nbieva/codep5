---
title: 10print
lang: fr-FR
---

# 10print

[10print](https://10print.org/) est une base de travail très intéressante pour la création de motifs. Deux éléments (ici slash et antislash) sont affichés alternativement, et ce de façon aléatoire (je ne sais pas si alternatif et aléatoire sont compatibles mais vous m'aurez compris..).

+ [https://10print.org/](https://10print.org/)

<a data-fancybox title="" href="/10print.png">![](/assets/10print.png)</a>

<iframe width="740" height="490" style="border-radius:5px;" src="https://www.youtube.com/embed/m9joBLOZVEo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](https://www.makeartwithpython.com/images/10_print/10print.png)

Notez que cet exercice n'est qu'une base. Une fois la logique mise en place, **à vous de l'enrichir en modifiant les variables graphiques, en modifiant les éléments qui se répètent, etc..** En effet, nous ne sommes pas obligés d'alterner aléatoirement entre un slash et un anti-slash. Nous pourrions tout aussi bien alterner entre un cercle rouge et un carré bleu (l'exemple n'est pas très original.. A vous d'en imaginer de plus intéressants...), ou tout autre élément.

> Source: [https://www.makeartwithpython.com/blog/10-print-in-python/](https://www.makeartwithpython.com/blog/10-print-in-python/)

### En pratique

Essayez de créer 3 visuels sur la base de ce code, en le modifiant comme bon vous semble. Exportez-les en intégrant la fonction **save()** à votre code (voir ci-dessous) et envoyez-les moi par email sur nicolas.bieva@lacambre.be (nommez vos fichiers avec vos nom et prénom).

### Liens intéressants

+ Le site de 10print.org: [https://10print.org/](https://10print.org/)
+ Sur les pavages de Truchet: [https://pelletierauger.com/fr/projets/les-pavages-de-truchet.html](https://pelletierauger.com/fr/projets/les-pavages-de-truchet.html)
+ Encore une: [https://images.math.cnrs.fr/Les-pavages-de-Truchet.html](https://images.math.cnrs.fr/Les-pavages-de-Truchet.html)
+ Une demo: [https://editor.p5js.org/nicobiev/present/YyKwIMYk1](https://editor.p5js.org/nicobiev/present/YyKwIMYk1)
+ Une autre façon de s'y prendre pour obtenir un résultat semblable: [https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX](https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX)
+ La fonction line: [https://p5js.org/reference/#/p5/line](https://p5js.org/reference/#/p5/line)
+ Mais encore.. ce site où vous trouverez une foule d'emple de code intéressants: [http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)