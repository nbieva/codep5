---
title: Créez une horloge
lang: fr-FR
---

# Créez une horloge

P5js et Processing ont tous deux plusieurs fonctions liées au temps et permettant de récupérer en temps réel l'heure ( **hour()** ), la minute ( **minute()** ), la seconde ( **second()** ) et la milliseconde ( **millis()** ).

A partir de là, vous pouvez tout imaginer pour créer une sorte d'horloge pour visualiser le temps qui passe d'une façon inédite. Cela peut-être créer un sketch purement visuel, ou manipuler les valeurs numériques (ce que renvoient ces fonctions) pour brouiller les pistes, jouer avec les fuseaux (décalage) horaires, etc..

Vous trouverez ci-dessous un code d'exemple (qui n'est certainement pas un modèle) avec ces différentes fonctions.

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/nicobiev/embed/cWneuXsNd"></iframe>

```javascript
function setup() {
  createCanvas(800,600);
  noStroke();
}

function draw() {
  background(0, 122, 142);
  fill(76, 165, 71);
  rect(0,0,hour()*(width/24),height/3);
  fill(61, 121, 87);
  rect(0,height/3,minute()*(width/60),height/3);
  fill(64, 94, 104);
  rect(0,2*(height/3),second()*(width/60),height/3);
}
```

## Quelques liens

+ [Babylonian hours](https://www.babylonianhours.com/) + Docs
+ Other time: [https://otherti.me/](https://otherti.me/)
+ [http://golancourses.net/2015/lectures/visualizing-time/](http://golancourses.net/2015/lectures/visualizing-time/)
+ https://www.courses.tegabrain.com/cc18/visualizing-time/
+ [History of timekeeping devices](https://en.wikipedia.org/wiki/History_of_timekeeping_devices)
+ http://cmuems.com/2016/60212/lectures/lecture-09-09b-clocks/
+ http://cmuems.com/2016/60212/resources/drucker_timekeeping.pdf
+ [An entire history of time measurement in six minutes.](https://www.youtube.com/watch?v=SsULOvIWSUo) (frome [here](http://cmuems.com/2016/60212/deliverables/deliverables-02/)) !!!
+ [https://www.humanclock.com/](https://www.humanclock.com/)
+ Moment and P5js: [https://editor.p5js.org/kjhollen/sketches/B1t0ov4ab](https://editor.p5js.org/kjhollen/sketches/B1t0ov4ab)
+ [https://editor.p5js.org/nicobiev/present/cWneuXsNd](https://editor.p5js.org/nicobiev/present/cWneuXsNd)
+ [https://editor.p5js.org/NicolasB3/present/BhPqaJbNt](https://editor.p5js.org/NicolasB3/present/BhPqaJbNt)